'use strict'

module.exports = async function (fastify, opts) {
  fastify.get('/', async function (request, reply) {
    const CLI = fastify.initStorageCli();
    const providerInfo = await CLI.getProviderInfo({ contracts: false, balances: false });
    return providerInfo;
  })
}
