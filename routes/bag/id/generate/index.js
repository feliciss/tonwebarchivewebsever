'use strict'
const { mkdtemp, rm, mkdir, rename } = require('node:fs/promises');
const { tmpdir } = require('os');
const { sep } = require('node:path');

module.exports = async function (fastify, opts) {
  fastify.post('/', async function (request, reply) {
    // init storage cli
    const CLI = fastify.initStorageCli();
    try {
        // stores files to tmp dir and return paths
        const tmpDir = tmpdir();
        const tempFilePath = await mkdtemp(`${tmpDir}${sep}`);
        const files = await request.saveRequestFiles({ preservePath: true, tmpdir: tempFilePath }) // saves to tempFilePath folder
        for (const f of files) {
          const filename = f.filename; // filename includes full path
          const filepath = f.filepath;
          const filenameWithoutRoot = filename.substring(filename.indexOf('/') + 1);
          const subfolderMatched = filenameWithoutRoot.match(/.*\/.*/g);
          if (subfolderMatched) {
            const subfolderPaths = filenameWithoutRoot.substring(0, filenameWithoutRoot.lastIndexOf('/'));
            const createdSubfolder = await mkdir(`${tempFilePath}${sep}${subfolderPaths}`, { recursive: true });
            const filenameWithoutPaths = filename.substring(filename.lastIndexOf('/') + 1);
            await rename(filepath, `${createdSubfolder}${sep}${filenameWithoutPaths}`)
          } else {
            await rename(filepath, `${tempFilePath}${sep}${filenameWithoutRoot}`)
          }
        }
        const createResult = await CLI.create(tempFilePath, { upload: true, copy: true });
        // tmp files cleaned up on disk automatically
        await reply.send(createResult);
        // remove tmp files path on storage manually
        await rm(tempFilePath, { recursive: true, force: true });
        return reply
    } catch (err) {
        await reply.send({ ok: false, error: `error: ${err.message}`, code: 400 })
        return reply
    }
  })
}