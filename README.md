# The Open Network Web Archive Web Server

The Open Network Web Archive Web Server (TON Web Archive Web Server) was created using [Fastify-CLI](https://www.npmjs.com/package/fastify-cli) with tunneling provided by [PageKite](https://pagekite.net).

## Fastify-CLI

### Available Scripts

In the project directory, you can run:

#### `npm run dev`

Starts the app in development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `npm start`

Starts the app in production mode.

#### `npm run test`

Runs the test cases.

### Learn More

To learn more about Fastify, check out the [Fastify documentation](https://www.fastify.io/docs/latest/).

## PageKite

### URL

The PageKite URL for this project is: https://twaws.pagekite.me.

## Backend

The TON Web Archive Web Server utilizes [tonstorage-cli](https://github.com/ndatg/tonstorage-cli), which is a Node.js port of the command-line interface [storage-daemon-cli](https://github.com/ton-blockchain/ton/blob/master/storage/storage-daemon/storage-daemon-cli.cpp) and connects with [storage-daemon](https://github.com/ton-blockchain/ton/blob/master/storage/storage-daemon/storage-daemon.cpp).

### Binaries

Storage-daemon and storage-daemon-cli can be downloaded from the TON GitHub releases page:

```
https://github.com/ton-blockchain/ton/releases
```