'use strict'

const fp = require('fastify-plugin')

module.exports = fp(async function (fastify, opts) {
    fastify.register(require('@fastify/multipart'), {
        limits: {
          fileSize: Number(process.env.FILE_SIZE),  // For multipart forms, the max file size in bytes
        }
    });
})
